from django.shortcuts import render, redirect
from .models import Schedule as jadwal
from .forms import SchedForm

# Create your views here.
def Join(request):
    if request.method == "POST" :
        form = SchedForm(request.POST)
        if form.is_valid():
            sched = jadwal()
            sched.Name = form.cleaned_data['Name']
            sched.Lecturer = form.cleaned_data['Lecturer']
            sched.SKS = form.cleaned_data['SKS']
            sched.Description = form.cleaned_data['Description']
            sched.Semester = form.cleaned_data['Semester']
            sched.Class = form.cleaned_data['Class']
            sched.save()
        return redirect('/schedule')
    else :
        sched = jadwal.objects.all()
        form = SchedForm()
        response = {"sched":sched, 'form':form}
        return render(request,'schedule.html',response)

def delete(request):
    if request.method == "POST" :
        pk_matkul = request.POST['pk_matkul']
        jadwal.objects.get(pk_matkul=pk_matkul).delete()
        return redirect('/schedule')
    else :
        sched = jadwal.objects.all()
        form = SchedForm()
        response = {"sched":sched, 'form':form}
        return render(request, "schedule.html", response)

def see(request):
    if request.method == "POST" :
        pk_matkul = request.POST['pk_matkul']
        sched = jadwal.objects.get(pk_matkul=pk_matkul)
        response = {"sched":sched}
        return render(request, "isi.html", response)