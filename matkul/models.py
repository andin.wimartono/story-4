from django.db import models

# Create your models here.
class Schedule(models.Model):
    Name = models.CharField(max_length=50)
    Lecturer = models.CharField(max_length=50)
    SKS = models.IntegerField(blank=False)
    Description = models.CharField(max_length=100)
    Semester = models.CharField(max_length=50)
    Class = models.CharField(max_length=50)