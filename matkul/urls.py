from django.urls import path
from . import views

app_name = 'matkul'

urlpatterns = [
    path('', views.SchedForm, name='formMatkul'),
    path('join/', views.Join, name='Join'),
    path('delete/<str:pk_matkul>', views.delete, name='delete'),
    path('<str:pk_matkul>', views.see, name='see'),
]