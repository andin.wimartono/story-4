from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')

def landingPage(request):
    return render(request, 'landingpage.html')

def gallery(request):
    return render(request, 'gallery.html')

def schedule(request):
    return render(request, 'schedule.html')

def isi(request):
    return render(request, 'isi.html')