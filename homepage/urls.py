from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.landingPage, name='landingPage'),
    path('index/', views.index, name='index'),
    path('gallery/', views.gallery, name='gallery'),
    path('schedule/setyours/', views.schedule, name='schedule'),
    path('schedule/seethesched', views.isi, name='isi'),
]